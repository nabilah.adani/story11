from django.urls import path
from .views import *

urlpatterns = [
    path('', profile, name='profile'),
    path('booklist', booklist, name='booklist'),
    path('registration', registration, name='registration'),
    path('new/', new, name = 'new'),
    path('email_checking/', email_checking, name = 'email_checking'),
]