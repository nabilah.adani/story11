from django.db import models

class Subscriber(models.Model):
    name = models.CharField(max_length = 40)
    email = models.EmailField()
    password = models.CharField(max_length = 20)
