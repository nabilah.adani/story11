from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
# Create your views here.
def profile(request):
    return render(request, 'profile.html')

def booklist(request):
    return render(request, 'booklist.html')

def registration(request):
    return render(request, 'registration.html')

def new(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']

        new_subscriber = Subscriber.objects.create(
            name = name,
            email = email,
            password = password,
        )
        new_subscriber.save()

        return HttpResponse('')
    else:
        return HttpResponseRedirect('/registration/')
    
def email_checking(request):
    emailIsExist = False
    if 'email' in request.POST:
        count = Subscriber.objects.filter(email = request.POST['email']).count()
        if count > 0: emailIsExist = True
    return JsonResponse({"email_is_exist": emailIsExist}, content_type = 'application/json')
