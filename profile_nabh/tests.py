from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
import profile_nabh.views as views
from .models import Subscriber

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Story11UnitTest(TestCase):

    def test_story10_registration_url_is_exist(self):
        response = Client().get('/registration/')
        self.assertEqual(response.status_code, 200)

    def test_story10_email_checking_url_is_exist(self):
        response = Client().get('/registration/email_checking/')
        self.assertEqual(response.status_code, 200)

    def test_story10_new_email_url_is_exist(self):
        response = Client().get('/registration/new/')
        self.assertEqual(response.status_code, 302)

    def test_story10_registration_using_index_func(self):
        found = resolve('/registration/')
        self.assertEqual(found.func, views.index)

    def test_story10_email_checking_using_email_checking_func(self):
        found = resolve('/registration/email_checking/')
        self.assertEqual(found.func, views.email_checking)

    def test_story10_new_email_using_new_email_func(self):
        found = resolve('/registration/new/')
        self.assertEqual(found.func, views.new)
        
    def test_story10_using_registration_page_template(self):
        response = Client().get('/registration/')
        self.assertTemplateUsed(response, 'lab_10/registration.html')

    def test_story10_registration_page_is_complete(self):
        request = HttpRequest()
        response = views.index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Registration", html_response)
        self.assertIn("Your name", html_response)
        self.assertIn("Email address", html_response)
        self.assertIn("Password", html_response)

    def test_model_subscriber_can_add_new_status(self):
        new_subscriber = Subscriber.objects.create(name = 'cloud', email = 'test@gmail.com', password = 'test')
        counting_added_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_added_subscriber, 1)

    def test_story10_post_success_and_render_the_result(self):
        response_post = Client().post('/registration/new/', {'name': 'cloud', 'email': 'test@gmail.com', 'password': 'test'})
        self.assertEqual(response_post.status_code, 200)

    def test_story10_post_check_email(self):
        response_post = Client().post('/registration/email_checking/', {'name': 'cloud', 'email': 'test@gmail.com', 'password': 'test'})
        self.assertEqual(response_post.status_code, 200)
