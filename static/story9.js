$(document).ready(function () {
    $.ajax({ 
        url : "/book_data/" + "quilting", 
        dataType : "json",
        success: function (response) { 
            $('#table-body').empty()
            console.log("success");
            var book_data = response["data"];
            for (var i = 0; i < book_data.length; i++) {
                var table_content = "<tr>" +
                    "<td>" + '<img src="'+ book_data[i]['cover'] +'" class="img-fluid rounded mx-auto d-block">' + "</td>" +
                    "<th scope=\"row\">" + book_data[i]['title'] + "</th>" +
                    "<td>" + book_data[i]['authors'] + "</td>" +
                    "<td>" + book_data[i]['year_published'] + "</td>" +
                    "<td><p class=\"favorite star-symbol\">☆</p></td>" +
                "</tr>";
                $('#table-body').append(table_content);
            };
        },
        
        error: function (error) {
            var table_content = "<tr><td colspan=\"5\" class = \"text-center\">JSON failed to loaded :(</td></tr>";
            $('#table-body').append(table_content);
        },
    });

    $(document).on('click', '#button-search', function(){
        $.ajax({ 
        url : "/book_data/" + $('#text-search').val(),
        dataType : "json",
        success: function (response) { 
            $('#table-body').empty()
            console.log("success");
            var book_data = response["data"];
            for (var i = 0; i < book_data.length; i++) {
                var table_content = "<tr>" +
                    "<td>" + '<img src="'+ book_data[i]['cover'] +'" class="img-fluid rounded mx-auto d-block">' + "</td>" +
                    "<th scope=\"row\">" + book_data[i]['title'] + "</th>" +
                    "<td>" + book_data[i]['authors'] + "</td>" +
                    "<td>" + book_data[i]['year_published'] + "</td>" +
                    "<td><p class=\"favorite star-symbol\">☆</p></td>" +
                "</tr>";
                $('#table-body').append(table_content);
            };
        },
        
        error: function (error) {
            $('#table-body').empty()
            var table_content = "<tr><td colspan=\"5\" class = \"text-center\">JSON failed to loaded :(</td></tr>";
            $('#table-body').append(table_content);
        },
    });
        
    });

    var count_favorite = 0;
    update_favorite();
    function update_favorite() {
        $('#favorite-count').text(count_favorite);
    };

    $(document).on('click', '.favorite', function(){
        if($(this).text() == "☆"){
            count_favorite++;
            update_favorite();
            $(this).text("★");
        }
        else{
            count_favorite--;
            update_favorite();
            $(this).text("☆");
        }
    });

    $('#hide-book').click(function() {
        var book_list = document.getElementById("book-list");
        book_list.style.visibility = "hidden";
    });

    $('#show-book').click(function() {
        var book_list = document.getElementById("book-list");
        book_list.style.visibility = "visible";
    });

    $('#light-on').click(function() {
        $('body').css({
            'background-color': 'white'
        });
        $('.container-fluid').css({
            'background-color': '#FFA411',
            'color':'black'
        });
        $('#footer').css({
            'color':'black'
        });
    });
    $('#dark-on').click(function() {
        $('body').css({
            'background-color': '#01161e'
        });
        $('.container-fluid').css({
            'background-color': '#086788',
            'color':'white'
        });
        $('#footer').css({
            'color':'white'
        });
    });

    $( "#accordion" ).accordion({
        active: false,
        collapsible: true,
        heightStyle: "content"
    });
    
    var collapsible = $( "#accordion" ).accordion( "option", "collapsible" );
    $( "#accordion" ).accordion( "option", "collapsible", true );

    var heightStyle = $( "#accordion" ).accordion( "option", "heightStyle" );
    $( "#accordion" ).accordion( "option", "heightStyle", "content" );

});
